var global_r = 1;

function rChanger(input) {
    let val = parseFloat(input.value);
    if (isNaN(val)) {
        return;
    }
    document.getElementById('Rf').value=val;
    let ingredients = document.querySelectorAll('input.R');
    for(let i = 0; i < ingredients.length; i++) {
        ingredients[i].checked  = false;
    }
    input.checked  = true;

    let plot = document.getElementById("svg_plot");
    let toRemove = [];
    plot.childNodes.forEach(function (childNode) {
        if (childNode.nodeName === "circle")
            toRemove.push(childNode);
        if (childNode.nodeName === "text") {
            let temp_r = val;
            if ([].includes.call(document.getElementsByClassName("divis"), childNode)) {
                temp_r = val / 2;
            }
            childNode.childNodes.forEach(function (child) {
                if (child.nodeName === "tspan") {
                    let tspan = document.createElementNS("http://www.w3.org/2000/svg", 'tspan');
                    tspan.innerHTML = temp_r;
                    childNode.replaceChild(tspan, child);
                }
            });
        }
    });
    toRemove.forEach(function (childNode) {
        childNode.setAttribute('cx', convertXr(childNode.getAttribute('cx'), global_r, val));
        childNode.setAttribute('cy', convertYr(childNode.getAttribute('cy'), global_r, val));
        if (+childNode.ownR === +val) {
            childNode.setAttribute('fill-opacity', "1");
            childNode.setAttribute('stroke-opacity', "1");
        } else {
            childNode.setAttribute('fill-opacity', "0.2");
            childNode.setAttribute('stroke-opacity', "0.5");
        }
    });
    global_r = val;
}

function draw_point(point) {
    let plot = document.getElementById("svg_plot");
    let circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
    circle.setAttribute('cx', convertX(+point.x));
    circle.setAttribute('cy', convertY(+point.y));
    circle.setAttribute('r', "4");
    circle.setAttribute("fill", point.success ? "#00ff00" : "#ff0000");
    circle.setAttribute("stroke-width", "1");
    circle.setAttribute("stroke", "black");
    circle.ownR = point.r;
    plot.appendChild(circle);
    circle.id = point.id;
    if (+circle.ownR !== +global_r) {
        circle.setAttribute('fill-opacity', "0.2");
        circle.setAttribute('stroke-opacity', "0.5");
    }
}

function plotClicked(event){
    if (document.elementFromPoint(event.clientX, event.clientY).tagName !== "circle") {
        let r = global_r;
        let oX = convertXReverse(event.offsetX);
        let oY = convertYReverse(event.offsetY);
        document.getElementById("X_h").setAttribute("value", oX);
        document.getElementById("X_h").checked = true;
        document.getElementById("Y").setAttribute("value", oY);
        document.getElementById("Rf").setAttribute("value", r);
        document.getElementById("submit_form").click();
        // $.get( "check", { R: r, X: oX, Y:oY } );
    }
}

function convertXReverse(cx) {
    return ((cx - 175) * global_r / 140).toFixed(3);
}

function convertYReverse(cy) {
    return ((cy - 175) * global_r / -140).toFixed(3);
}


function convertX(x) {
    return (120 * x / + global_r + 150).toFixed(3);
}

function convertY(y) {
    return (150 - 120 * y / + global_r).toFixed(3);
}

function convertXr(x, r1, r2) {
    return (((x - 150) * r1)/ r2 + 150).toFixed(3);
}

function convertYr(y, r1, r2) {
    return (150 + ((y - 150) * r1)/ r2).toFixed(3);
}
