<%@ page import="Main.Point" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: dunaev
  Date: 04/11/18
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ServletContext servletContext = request.getServletContext();
%>
<html>
<head>
    <title>lw2</title>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jcanvas/21.0.1/jcanvas.js"
            integrity="sha256-ZqFgz7QUEDRCvHiosY/FIao3Zg5Pmg2J4rlLTEWu4BI=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/main.css">

    <script src="js/chart.js"></script>

    <link rel="shortcut icon" href="img/duck_mini.png" sizes="">

</head>
<body>
    <header id="header">
        <p>Выполнил: Дунаев&nbsp;Алексей</p>
        <p>P3217 Вариант:&nbsp;14135484</p>
    </header>
    <div id="plot" class="element" title="Plot">
        <div>
            <svg id="svg_plot" xmlns="http://www.w3.org/2000/svg" width="350" viewBox="0 0 300 300"
                 onclick="plotClicked(evt)">
                <defs>
                    <style>
                        .cls-2 {
                            fill: none;
                        }

                        .cls-2, .cls-3 {
                            stroke: #000;
                            stroke-miterlimit: 10;
                            stroke-width: 0.5px;
                        }

                        .cls-4 {
                            font-size: 12px;
                        }

                        .cls-4, .cls-5 {
                            font-family: MyriadPro-Regular, Myriad Pro, serif;
                        }

                        .cls-5 {
                            font-size: 14px;
                        }</style>
                </defs>
                <title>Graph</title>
                <path class="cls-1"
                      d="M150 150 L150 30 A120,120,0,0,0,30,150 L150 150 Z"
                      transform="translate(0 0)"></path>
                <polygon class="cls-1" points="30,150 150,150 150,210"></polygon>
                <rect class="cls-1" x="150" y="150" width="120" height="60"></rect>
                <line class="cls-2" x1="150" y1="300" x2="150" y2="5.295"></line>
                <polygon points="150 0 153.049 7.462 150 5.691 146.952 7.462 150 0"></polygon>
                <line class="cls-2" y1="150.052" x2="294.705" y2="149.95"></line>
                <polygon points="300 149.948 292.539 153 294.309 149.95 292.537 146.902 300 149.948"></polygon>
                <path class="cls-3" d="M90.02,150l-.02.02V150Z" transform="translate(0 0)"></path>
                <line class="cls-2" x1="210" y1="154" x2="210" y2="146"></line>
                <line class="cls-2" x1="154" y1="210" x2="146" y2="210"></line>
                <line class="cls-2" x1="154" y1="90" x2="146" y2="90"></line>
                <line class="cls-2" x1="90" y1="154" x2="90" y2="146"></line>
                <line class="cls-2" x1="154" y1="270" x2="146" y2="270"></line>
                <line class="cls-2" x1="270" y1="154" x2="270" y2="146"></line>
                <line class="cls-2" x1="154" y1="30" x2="146" y2="30"></line>
                <line class="cls-2" x1="30" y1="154" x2="30" y2="146"></line>
                <text class="cls-4" transform="translate(30 143)">-
                    <tspan>1</tspan>
                </text>
                <text class="cls-4" transform="translate(154 267)">-
                    <tspan>1</tspan>
                </text>
                <text class="cls-4" transform="translate(263 143)">
                    <tspan>1</tspan>
                </text>
                <text class="cls-4" transform="translate(158 40.045)">
                    <tspan>1</tspan>
                </text>
                <text class="cls-4 divis" transform="translate(154 207)">-
                    <tspan>0.5</tspan>
                </text>
                <text class="cls-4 divis" transform="translate(158 99.974)">
                    <tspan>0.5</tspan>
                </text>
                <text class="cls-4 divis" transform="translate(210 143)">
                    <tspan>0.5</tspan>
                </text>
                <text class="cls-4 divis" transform="translate(69 143)">-
                    <tspan>0.5</tspan>
                </text>
                <text class="cls-5" transform="translate(158 12.378)">Y</text>
                <text class="cls-5" transform="translate(288.666 143)">X</text>
                <%--<%--%>
                    <%--if (session == null) {--%>
                        <%--return;--%>
                    <%--}--%>

                    <%--ArrayList<Point> pointsList = (ArrayList<Point>) servletContext.getAttribute("Points");--%>
                <%--%>--%>
                <%--<% for (Point point : pointsList) { %>--%>
                <%--<circle cx="<%= (120 * point.x / + point.r + 150) %>" cy="<%= (150 - 120 * point.y / + point.r) %>" r="4" fill="<%= point.success==1 ? "#00ff00" : "#ff0000"%>" fill-opacity="0.2" stroke-opacity="0.5" stroke-width="1" stroke="black" id="0"></circle>--%>
                <%--<%}%>--%>
            </svg>
        </div>
    </div>
    <div id="content">
        <form method="GET" onsubmit="return validate()" id="form" action="${pageContext.request.contextPath}/check">
            <label>
                X:
                <label><input name="X" type="radio" value="-3"> -3</label>
                <label><input name="X" type="radio" value="-2"> -2</label>
                <label><input name="X" type="radio" value="-1"> -1</label>
                <label><input name="X" type="radio" id="X_h" value="0" checked> 0</label>
                <label><input name="X" type="radio" value="1"> 1</label>
                <label><input name="X" type="radio" value="2"> 2</label>
                <label><input name="X" type="radio" value="3"> 3</label>
                <label><input name="X" type="radio" value="4"> 4</label>
                <label><input name="X" type="radio" value="5"> 5</label>
                <label><input name="X" type="radio" value="6"> 6</label>
            </label>
            <p> <label for="Y">Y: </label><input id="Y" name="Y" placeholder="Введите валидное число" class="Y" maxlength="9" type="text" required>
            </p>
            <p><label for="Rf" class="Rlabel">R:</label><input type="text" name="R" id="Rf" class="Rf" value="1" readonly required>
                <br/>
                <script type="text/javascript">

                </script>
                <label>
                    <input type="checkbox" name="R" class="R" onclick="rChanger(this)" value="1" checked>
                    1
                </label>
                <label>
                    <input type="checkbox" name="R" class="R" onclick="rChanger(this)" value="2">
                    2
                </label>
                <label>
                    <input type="checkbox" name="R" class="R" onclick="rChanger(this)" value="3">
                    3
                </label>
                <label>
                    <input type="checkbox" name="R" class="R" onclick="rChanger(this)" value="4">
                    4
                </label>
                <label>
                    <input type="checkbox" name="R" class="R" onclick="rChanger(this)" value="5">
                    5
                </label>
            </p>
            <p><input type="submit" id="submit_form" class="submit" value="Отправить"> </p>
            <script type="text/javascript">
                function validate() {
                    document.getElementById('Y').style.borderColor = "white";
                    let Yval = parseFloat(document.getElementById('Y').value);
                    if(isNaN(Yval) || Yval >= 5 || Yval < -3 || Yval === -3) {
                        document.getElementById('Y').value="";
                        document.getElementById('Y').style.borderColor = "red";
                        return false;
                    }
                    return true;
                }
            </script>
        </form>
    </div>

    <%
        ArrayList<Point> pointsList = (ArrayList<Point>) servletContext.getAttribute("Points");
        if (pointsList != null) {
    %>
    <script>
        <%for (Point point : pointsList) {%>
            draw_point(<%=point.toString()%>);
        <%}}%>
    </script>

</body>
</html>
