<%--
  Created by IntelliJ IDEA.
  User: dunaev
  Date: 23/12/18
  Time: 21:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="Main.Point" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    ServletContext servletContext = request.getServletContext();
%>
<html>
<head>
    <title>Table</title>
</head>
<body>
    <div id="content">
        <table class="table table-sm table-hover">
            <thead>
            <tr>
                <th scope="col">X</th>
                <th scope="col">Y</th>
                <th scope="col">R</th>
                <th scope="col">Попадание</th>
            </tr>
            </thead>
            <tbody id="results">
            <%
                if (session == null) {
                    return;
                }

                ArrayList<Point> pointsList = (ArrayList<Point>) servletContext.getAttribute("Points");
            %>
            <% for (Point point : pointsList) { %>
            <tr>
                <td>
                    <%= point.x %>
                </td>
                <td>
                    <%= point.y %>
                </td>
                <td>
                    <%= point.r %>
                </td>
                <td>
                    <%= point.success %>
                </td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
    <a href="${pageContext.request.contextPath}/">Back</a>
</body>
</html>
