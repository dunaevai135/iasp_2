package Main;

public class Point {
    public int id;
    public float x;
    public float y;
    public float r;

    public int success;
    public long duration;
    public String date;


    Point(float x, float y, float r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "{\"id\":" + this.id + "," +
                "\"x\":" + this.x + "," +
                "\"y\":" + this.y + "," +
                "\"r\":\"" + this.r + "\"," +
                "\"success\":" + this.success +
                "}";
    }
}