package Main;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "Main.AreaCheckServlet")
public class AreaCheckServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mesg = "";

        float userR = 0;
        float userX = 0;
        float userY = 0;

        try {
            userR = Float.parseFloat(request.getParameter("R"));
            userX = Float.parseFloat(request.getParameter("X"));
            userY = Float.parseFloat(request.getParameter("Y"));
            System.out.println(userR +" "+ userX +" "+ userY);
        } catch (NumberFormatException e) {
            mesg = "ERROR: value not numeric";
        } catch (NullPointerException e) {
            mesg = "ERROR: some data was lost on the way.";
        }

        if ((userX < -3) || (userX > 6))
            mesg = " X should be in -3 - 6";

        if ((userR < 1) || (userR > 5))
            mesg = " R should be in 1 - 5";

        if(!mesg.equals("")) {
            response.sendError(400, mesg);
            return;
        }

        Figure figure = new Figure(userR);
        Point point;
        point = new Point(userX, userY, figure.r);
        point.success = figure.fits(point) ? 1 : 0;
        request.setAttribute("point", point);


        ServletContext servletContext = request.getServletContext();
        if (servletContext.getAttribute("Points") == null){
            servletContext.setAttribute("Points", new ArrayList<Point>());
        }

        ArrayList<Point> pointsList = (ArrayList<Point>) servletContext.getAttribute("Points");
        pointsList.add(point);
        point.id = pointsList.size()-1;

        request.getRequestDispatcher("/table.jsp").forward(request, response);
    }
}
