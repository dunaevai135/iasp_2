package Main;

import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet(name = "Main.ControllerServlet")
public class ControllerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final Map parameters = request.getParameterMap();

        System.out.println(parameters.values().toString());
        if (parameters.containsKey("X") &&
                parameters.containsKey("Y") &&
                parameters.containsKey("R")) {

            request.getRequestDispatcher("/check")
                    .forward(request, response);
            return;
        }

        request.getRequestDispatcher("/index.jsp")
                .forward(request, response);
//        response.getWriter().println("Unknown request target");
    }
}
